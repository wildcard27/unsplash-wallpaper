#!/bin/bash

while true; do
    # Move current image for safe keeping
    cp wallpaper.jpg saved/$RANDOM.jpg >/dev/null 2>&1

    # Remove current wallpaper file
    rm wallpaper.jpg >/dev/null 2>&1

    # Download random image from unsplash API
    wget https://unsplash.it/1920/1080/?random >/dev/null 2>&1

    # Convert to jpg
    mv ./index.html?random ./wallpaper.jpg >/dev/null 2>&1

    # Set as wallpaper
    feh --bg-scale wallpaper.jpg

    # Repeat in 5 mins
    sleep 300
done
