#!/bin/bash
for pid in $(pidof -x unsplash-wallpaper.sh); do
    if [ $pid != $$ ]; then
        kill -9 $pid
    fi 
done

while true; do

    filename=$RANDOM

    # Remove current wallpaper file
    rm wallpaper.jpg >/dev/null 2>&1

    # Download random image from unsplash API
    wget -P /home/matt/unsplash/ https://unsplash.it/1920/1080/?random >/dev/null 2>&1

    # Convert to jpg
    mv /home/matt/unsplash/index.html?random /home/matt/unsplash/wallpaper.jpg >/dev/null 2>&1

    # Move current image for safe keeping
    cp /home/matt/unsplash/wallpaper.jpg /home/matt/unsplash/saved/$filename.jpg >/dev/null 2>&1

    # Set as wallpaper
    gsettings set org.cinnamon.desktop.background picture-uri  "file:///home/matt/unsplash/saved/$filename.jpg"

    # Repeat in 5 mins
    sleep 300
done
